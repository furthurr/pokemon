import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import '../../router/resources.dart';

class DB  {

  static Future<Database>_openDB() async {
    return openDatabase(join(await getDatabasesPath(),'pokemon.db'),
      onCreate: (db,version){
        db.execute('CREATE TABLE equipos ( id INTEGER PRIMARY KEY,nombre TEXT, pokemones TEXT, identrenador INTEGER )');
        return db.execute('CREATE TABLE entrenadores ( id INTEGER PRIMARY KEY,nombre TEXT, status INTEGER ) ');
      },version:1);
  }

  static Future<void> insert(Entrenador entrenador) async{
    Database db = await _openDB();
    db.insert('entrenadores', entrenador.toJson());
    infoprint("nuevo usuario en DB");
    // await db.close();
  }

  static Future<void> delete(String entrenadorNombre) async{
    Database db = await _openDB();
    List<Map<String, dynamic>> entrenadores = await db.rawQuery("SELECT * FROM entrenadores WHERE nombre='$entrenadorNombre'");
    Entrenador entrenador = Entrenador.fromJson(entrenadores[0]);
    await db.rawQuery("DELETE FROM equipos WHERE identrenador=${entrenador.id}");
    await db.delete('entrenadores', where: 'nombre=?', whereArgs: [entrenadorNombre]);
    // await db.close();
  }

  static Future<void> update(Entrenador entrenador) async{
    Database db = await _openDB();
    db.update('entrenadores', entrenador.toJson(), where: 'id=?', whereArgs: [entrenador.id]);
    // await db.close();
  }

  static Future<List<Entrenador>> entrenadores() async{
    Database db = await _openDB();
    List<Map<String, dynamic>> entrenadoresDB = await db.query("entrenadores");
    List<Entrenador> entrenadores = List.generate(entrenadoresDB.length, (index) => 
      Entrenador.fromJson(entrenadoresDB[index])
    );
    infoprint(entrenadores);
    for (var entrenador in entrenadores) {
      infoprint("id: ${entrenador.id} nombre: ${entrenador.nombre} status: ${entrenador.status} ");
    }
    return entrenadores;
    // await db.close();
  }

  static Future<void> activarEntrenador(String entrenador)async {
    Database db = await _openDB();
    await db.rawQuery("UPDATE entrenadores SET status = true WHERE nombre='$entrenador'");
  }

  static Future<void> desactivaEntrenador(String entrenador)async {
    Database db = await _openDB();
    await db.rawQuery("UPDATE entrenadores SET status = false WHERE nombre='$entrenador'");
  }

  static Future<Entrenador> nuevoEntrenador(Entrenador entrenador)async {
    Database db = await _openDB();
    List<Map<String, dynamic>>entrenadores = await db.rawQuery("SELECT * FROM entrenadores WHERE nombre='${entrenador.nombre}'");
    infoprint(entrenadores);
    if (entrenadores.isEmpty){
      await insert(entrenador);
    }
    entrenadores = await db.rawQuery("SELECT * FROM entrenadores WHERE nombre='${entrenador.nombre}'");
    entrenador = Entrenador.fromJson(entrenadores[0]);
    return entrenador;
  }

  static Future<Entrenador> obtenerEntrenador() async{
    Database db = await _openDB();
    List<Map<String, dynamic>> entrenadores = await db.rawQuery("SELECT * FROM entrenadores WHERE status=1");
    Entrenador entrenador = Entrenador.fromJson(entrenadores[0]);
    return entrenador;
  }

  static Future<bool> entrenadorLogeado() async{
    Database db = await _openDB();
    List<Map<String, dynamic>> entrenadores = await db.rawQuery("SELECT * FROM entrenadores WHERE status=1");
    return entrenadores.isNotEmpty;
  }

  //Equipos
  static Future<void>  equiposInsert(String pokemones, String nombre) async{
    Database db = await _openDB();
    Entrenador entrenador = await DB.obtenerEntrenador();
    await db.rawQuery("INSERT INTO equipos (nombre, pokemones, identrenador) VALUES( '$nombre','$pokemones',${entrenador.id});");
    infoprint(" se inserto un nuevo equipo");
  }

  static Future<List<Equipo>> misEquipos() async{
      Database db = await _openDB();
      Entrenador entrenador = await DB.obtenerEntrenador();
      List<Map<String, dynamic>> equiposDB = await db.rawQuery("SELECT * FROM equipos WHERE identrenador = ${entrenador.id}");
      List<Equipo> equipos = List.generate(equiposDB.length, (index) => 
        Equipo.fromJson(equiposDB[index])
      );
      return equipos;
  }

  static Future<void> eliminaEquipo(int? idEquipo) async{
      Database db = await _openDB();
      await db.delete('equipos', where: 'id=?', whereArgs: [idEquipo]);
  }


}