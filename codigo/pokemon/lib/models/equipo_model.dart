
class Equipo{
  int? id;
  String? nombre;
  String? pokemones;
  int? identrenador;


  Equipo({this.id,this.nombre,this.pokemones,this.identrenador});

  Equipo.fromJson(Map<String, dynamic> json){

    id     = json['id'];
    nombre = json['nombre'];
    pokemones = json['pokemones'];
    identrenador = json['identrenador'];
  
  }


  Map<String, dynamic> toJson(){
    final json = <String, dynamic>{};

    json['id']     = id;
    json['nombre'] = nombre;
    json['pokemones'] = pokemones;
    json['identrenador'] = identrenador;

    return json;
  }


}