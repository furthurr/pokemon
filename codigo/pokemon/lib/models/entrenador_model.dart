
class Entrenador{
  int? id;
  String? nombre;
  bool? status;


  Entrenador({this.id,this.nombre,this.status});

  Entrenador.fromJson(Map<String, dynamic> json){

    id     = json['id'];
    nombre = json['nombre'];
    status = json['status']==1?true:false;

  }


  Map<String, dynamic> toJson(){
    final json = <String, dynamic>{};

    json['id']     = id;
    json['nombre'] = nombre;
    json['status'] = status;

    return json;
  }


}