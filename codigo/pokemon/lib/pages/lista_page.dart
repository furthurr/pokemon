import 'package:flutter/material.dart';

import '../router/resources.dart';

class ListaPage extends StatelessWidget {
  const ListaPage({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    final listacontroller = Provider.of<ListaController>(context);
    return  Scaffold(
      body: Stack(
        children: [
          Positioned(
            right: - sizedWith(context, 31),
            top: - sizedWith(context, 31),
            child: SafeArea(
              child: Image.asset('assets/imgs/pokeball_gray.png',
              width: sizedWith(context, 75),
              ),
            ),
          ),
          listacontroller.creandoGrupo?
          crearGrupoWidget(context, listacontroller):
          tituloWidget(context),
          Padding(
            padding: const EdgeInsets.only(top:140),
            child: SingleChildScrollView(
              child: FutureBuilder(
                future: listacontroller.listaPokemones,
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  return snapshot.hasData?
                  GridView.builder(
                  padding: const EdgeInsets.only(top:0,left: 10,right: 10),
                  physics: const NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                  ),
                  itemCount: snapshot.data.length, 
                  itemBuilder: (BuildContext context, index){
                    return Padding(
                      padding: EdgeInsets.all(sizedWith(context, 2)),
                      child: InkWell(
                        onTap: (){
                          infoprint("click pokemon");
                          listacontroller.creandoGrupo?
                          listacontroller.clickAgruparPokemon(index.toString()):
                          listacontroller.clickPokemon(context,index); 
                        },
                        child: pokemonWidget(listacontroller, context, snapshot.data[index], index),
                      ),
                    );
                  }
                )
                :Image.asset("assets/imgs/pokeball.gif");
                },
              ),
                
            ),
          ),
        ],
      ),
      //listacontroller?
      floatingActionButton: !listacontroller.creandoGrupo?
      FloatingActionButton(
        onPressed: ()=>listacontroller.clickCrearGrupo(),
        //backgroundColor: listacontroller.getColor(),
        elevation: 10,
        child: const Icon(
          Icons.format_list_numbered_rounded,
        ),  
      ):null
    );
  }

  Container pokemonWidget(ListaController listacontroller, BuildContext context, String data, int index) {
    return Container( 
      decoration:  BoxDecoration( 
        //color: listacontroller.getColor(),
        gradient: const LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            AppTheme.azul,
            AppTheme.verde,
            AppTheme.amarillo,
            AppTheme.rojo,
            AppTheme.morado,
            AppTheme.cafe,

          ],
        ),
        borderRadius: BorderRadius.circular(20),
      ),
      child: Stack(
        children: [
          Positioned(
            bottom: -10,
            left: -10,
            child: Image.asset("assets/imgs/pokeball_gray.png",
              width: sizedWith(context, 25),
            )
          ),
          Positioned(
            bottom: 0,
            right: 0,
            child: Hero(
              tag: index+1,
              child: CachedNetworkImage(
                width: sizedWith(context, 40), 
                imageUrl: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${index+1}.png",              
                placeholder: (context, url) => const Center(child:  Text("Cargando...",style:TextStyle(color: Colors.white),)),
                errorWidget: (context, url, error) => const Icon(Icons.error),
              ),
            ),
          ),
        
          Container(
            height: 40,
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20),
                topRight: Radius.circular(20)
              ),
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Color.fromARGB(126, 0, 0, 0),Color.fromARGB(0, 0, 0, 0)],
              )
            ),
          ),
          Positioned(
            top: 10,
            left: 10,
            child: Text(data,
              style: AppTheme.lblTitle,
            ),
          ),
          Positioned(
            top: 5,
            right: 5,
            child: 
            Icon(Icons.add_circle,
            color: listacontroller.grupoPokemon.contains(index.toString())?Colors.greenAccent:Colors.transparent,
            size: 38,)
            ,
          ),
        ],
      ),
    );
  }

  Positioned tituloWidget(BuildContext context) {
    return const Positioned(
      top: 100,
      left: 20,
      child: Text('Pokemones',
              style: AppTheme.lblTitlePage,
              ),
    );
  }

  SizedBox crearGrupoWidget(BuildContext context,ListaController listacontroller) {
    return SizedBox(
      height: 130,
      width: sizedWith(context, 100),                
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children:  [
          const Expanded(child: SizedBox()),
          Padding(
            padding: EdgeInsets.only(left: sizedWith(context, 5),right: sizedWith(context, 5)),
            child: Row(
              children:  [
                ElevatedButton(
                  onPressed: (){listacontroller.clickCrearGrupo();}, 
                  child: const Text("X"),
                  style: ElevatedButton.styleFrom(
                    primary: AppTheme.rojo,
                    minimumSize: const Size(40, 40),
                  ),   
                ), 
                const SizedBox(width: 5,),
                Expanded(
                  child: TextField(
                    onChanged: (value) => listacontroller.actualizaNombreGrupo(value),
                    style: AppTheme.lblNormal,                            
                    decoration: const InputDecoration(
                      hintStyle: AppTheme.lblNormal2,
                      hintText: "Nombre de grupo"
                    ),
                  ),
                ),
                const SizedBox(width: 5,),
                ElevatedButton(
                  onPressed: () => listacontroller.clickGuardarGrupo(context), 
                  child: const Text("CREAR"),
                  style: ElevatedButton.styleFrom(
                    primary: AppTheme.verde,
                    minimumSize:  Size(sizedWith(context, 25), 40),
                  ),   
                ),
                const SizedBox(width: 5,),
              ],
            ),
          )
        ],
      ),
    );
  }
}