import 'package:flutter/material.dart';

import '../router/resources.dart';

class Homepage extends StatelessWidget {
  
  const Homepage({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
  final GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();
  final homecontroller = Provider.of<HomeController>(context);

    return  Scaffold(
      key: _scaffold,
      endDrawer: Drawer(
        child: Stack(
          children: [
            Container(
              decoration:const BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image:  AssetImage('assets/imgs/home/backmenu.jpeg')
                ),
              ),
            ),
            Container(
              color: const Color.fromARGB(185, 0, 0, 0)            
            ),
            Column(
              children:  [
                const SizedBox( height: 60,),
                const CircleAvatar(
                  backgroundColor: AppTheme.gris1,
                  backgroundImage: AssetImage('assets/imgs/home/pokeball.png'
                  ),
                  radius: 60,
                ),
                const SizedBox(height: 20,),
                FutureBuilder(
                  future: homecontroller.nombreEntrenador,
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    return snapshot.hasData?
                    Text(snapshot.data, style:AppTheme.lblTitle):
                    const Text('Consultando...',style:AppTheme.lblTitle);
                  },
                ),
                
                const SizedBox(height: 20),
                const Divider(
                  height: 1,
                  color: AppTheme.gris3
                ),
                //opciones 
                optionMenuWidget(context,'Pokemones',homecontroller),
                optionMenuWidget(context,'Equipos',homecontroller),
                Expanded(child: Container()),
                optionSalirWidget(context,homecontroller),
                const SizedBox(height: 20),
              ],
            )
          ],
        ),
      ),
      body: Stack(
        children: [
          homecontroller.vistaSeleccionada =='Pokemones'? 
          const ListaPage():const GrupoPage(),
          SafeArea(
            child: Row(
              children: [
                const Expanded(child: SizedBox()),
                IconButton(
                  onPressed: (){
                    _scaffold.currentState!.openEndDrawer();
                  }, 
                  icon: const Icon(Icons.menu)
                ),
              ],
            ),
          ),
        ],
      )
    );
  }

  Row optionSalirWidget(BuildContext context, HomeController homecontroller ) {
    return Row(
      children: [
        const Expanded(child: SizedBox()),
        SizedBox(
          height: 80,
          child: InkWell(
            onTap: ()=> homecontroller.clickSalir(context),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: const [
                SizedBox(width: 30,height: 20,),
                Image(
                  height: 30,
                  image: AssetImage('assets/imgs/icon_logout.png')
                ),
                SizedBox(width: 30,height: 49,),
                Text('Salir',
                  style: AppTheme.lblHomeOption
                ),
              ],
            ),
          ),
        ),
        const Expanded(child: SizedBox()),
      ],
    );
  }

  InkWell optionMenuWidget(BuildContext context, String opcion, HomeController homecontroller) {
    return InkWell(
      onTap: () {
        homecontroller.clickOpcion(context, opcion);
      },
      child:   SizedBox(
        height: 50,
        child: Column(
          children: [
            Container(
              alignment: Alignment.center,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const SizedBox(width: 30),
                  const Image(
                    height: 30, 
                    image: AssetImage('assets/imgs/pokeball_gray.png')
                  ),
                  const SizedBox(width: 30,height: 49,),
                  Text(opcion,
                    style: AppTheme.lblHomeOption
                  ),
                ],
              ),
            ),
            const Divider(
              height: 1,
              color: AppTheme.gris3
            ),
          ],
        ),
      ),
    );
  }

  
}