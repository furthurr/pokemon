import 'package:flutter/material.dart';

import '../router/resources.dart';

class GrupoPage extends StatelessWidget {
  
  const GrupoPage({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    final grupoController = Provider.of<GrupoController>(context);
    var temp = 15;
    return  Scaffold(
      body:  Stack(
        children: [
          Positioned(
            right: - sizedWith(context, 31),
            top: - sizedWith(context, 31),
            child: SafeArea(
              child: Image.asset('assets/imgs/pokeball_gray.png',
              width: sizedWith(context, 75),
              ),
            ),
          ),
          tituloWidget(context),


          Padding(
            padding: const EdgeInsets.only(top:140),
            child: SingleChildScrollView(
              child: FutureBuilder(
                future: grupoController.grupos,
                builder: (BuildContext context, AsyncSnapshot<List<Equipo>> snapshot) {
                  return snapshot.hasData?
                  ListView.builder(                
                    padding: const EdgeInsets.only(top:0,left: 10,right: 10),
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: snapshot.data?.length, 
                    itemBuilder: (BuildContext context, index){
                      List<String> pokemsstring = snapshot.data![index].pokemones.toString().split(',');
                      List<int> pokemsint = pokemsstring.map(int.parse).toList();
                      return Card(
                        elevation: 5,
                        child: Column(
                          children: [
                            const SizedBox(height: 10,),
                            Row(
                              children: [
                                const SizedBox(width: 10),
                                Text(snapshot.data![index].nombre.toString(),
                                  style: AppTheme.lblNormal,
                                ),
                                const Expanded(child: SizedBox()),
                                FloatingActionButton.small(
                                  onPressed: (){ 
                                    grupoController.clickEliminar( context, snapshot.data![index].id );
                                  },
                                  backgroundColor: AppTheme.rojo,
                                  child: const Icon(Icons.delete_forever_outlined),
                                ),
                                const SizedBox(width: 10)
                              ],
                            ),
                            Row(
                              children: [
                                const Expanded(child: SizedBox()),
                                pokeimgWidget(pokemsint, context, temp, 0),
                                pokeimgWidget(pokemsint, context, temp, 1),
                                pokeimgWidget(pokemsint, context, temp, 2),
                                pokeimgWidget(pokemsint, context, temp, 3),
                                pokeimgWidget(pokemsint, context, temp, 4),
                                pokeimgWidget(pokemsint, context, temp, 5),

                                const Expanded(child: SizedBox()),
                
                              ],
                            ),
                            const SizedBox(height: 10,)
                          ],
                        ),
                      );
                    },
                  )
                  :Image.asset("assets/imgs/pokeball.gif");
                }),
              ),
          ),
        ],
      )
    );
  }

  CachedNetworkImage pokeimgWidget(List<int> pokemsint, BuildContext context, int temp, int posi) {
    return CachedNetworkImage(
      width: sizedWith(context, temp),
      imageUrl: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${pokemsint[posi]+1}.png",              
      placeholder: (context, url) => const Center(child:  Text("⏳",style:TextStyle(color: Colors.white),)),
      errorWidget: (context, url, error) => const Icon(Icons.error),
    );
  }

  Positioned tituloWidget(BuildContext context) {
    return const Positioned(
      top: 100,
      left: 20,
      child: Text('Equipos',
              style: AppTheme.lblTitlePage,
      ),
    );
  }
  
}