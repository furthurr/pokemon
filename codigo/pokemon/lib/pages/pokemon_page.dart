import 'package:flutter/material.dart';

import '../router/resources.dart';

class PokemonPage extends StatelessWidget {
  
  const PokemonPage({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    final pokemonController = Provider.of<PokemonController>(context);

    return  Scaffold(
      backgroundColor: pokemonController.scaffoldBack,
      appBar:  AppBar(
        leading: IconButton(
          onPressed: ()=>Navigator.pop(context), 
          icon: const Icon(Icons.arrow_back_rounded,color: Colors.white,)
        ),
        backgroundColor: Colors.transparent ,
      ),
      body: Stack(
        children: [
          Positioned(
            right: sizedWith(context, 0),
            top: sizedWith(context, 20),
            child: SafeArea(
              child: Image.asset('assets/imgs/pokeball_gray.png',
              width: sizedWith(context, 40),
              ),
            ),
          ),
          Positioned(
            top: 0,
            left: 20,
            child: Text(pokemonController.pokemon.name.toString(), style: AppTheme.lblTitlePage2,) 
          ),
          Positioned(
            top: 6,
            right: 20,
            child: Text("ID: "+pokemonController.pokemonIdselected.toString(),style: AppTheme.lblHomeOption),
          ),
                
          Positioned(
            bottom: 0,
            child: Container(
              width: sizedWith(context, 100),
              height: sizedHeight(context, 60),
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(topLeft: Radius.circular(30),topRight: Radius.circular(30)),
                color: Colors.white
              ),
              child: Column(
                children: [
                    SizedBox(height: sizedHeight(context, 3)),
                    opcionesWidget(context, pokemonController),
                    SizedBox(
                      height: sizedHeight(context, 45),
                      width: sizedWith(context, 90),
                      child: !pokemonController.stats?
                        datosWidget(context,pokemonController):
                        statsWidget(context,pokemonController),
                    )
                ],
              ),
            ),
          ),
          Positioned(
            bottom: sizedHeight(context, 55),
            child: SizedBox(
              width: sizedWith(context, 100),
              child: Center(
                child: Hero(
                  tag: pokemonController.pokemonIdselected,
                  child: 
                  CachedNetworkImage(
                      height: sizedHeight(context, 30), 
                      imageUrl: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${pokemonController.pokemonIdselected}.png",              
                      placeholder: (context, url) => const Center(child:  Text("Cargando...",style:TextStyle(color: Colors.white),)),
                      errorWidget: (context, url, error) => const Icon(Icons.error),
                    ),
                  // Image.network("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${pokemonController.pokemonIdselected}.png",
                  //   height: sizedHeight(context, 30),                              
                  // ),
                ),
              ),
            ),
          ),
        ],
      )
    );
  }

  SingleChildScrollView datosWidget(BuildContext context, PokemonController pokemonController) {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(height: sizedHeight(context, 3),),
          Row(
            children: [
              const Expanded(child: SizedBox()),
              Image.network("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/versions/generation-v/black-white/animated/${pokemonController.pokemonIdselected}.gif"),
              const Expanded(child: SizedBox()),
              Image.network("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/versions/generation-v/black-white/animated/back/${pokemonController.pokemonIdselected}.gif"),
              const Expanded(child: SizedBox()),
            ],
          ),
          SizedBox(height: sizedHeight(context, 3),),
          Card(
            elevation: 5,
            child: Column(
              children: [
                SizedBox(height: sizedHeight(context, 3),),
                Row(
                  children: [
                    const Expanded(child: SizedBox()),
                    datoWindget('Peso',(pokemonController.pokemon.weight!/10).toString()+" Kg"),
                    const Expanded(child: SizedBox()),
                    datoWindget('Estatura',(pokemonController.pokemon.height!/10).toString()+' Mts'),
                    const Expanded(child: SizedBox()),
                  ],
                ),
                SizedBox(height: sizedHeight(context, 1),),
                const Divider(),
                SizedBox(height: sizedHeight(context, 1),),
                Row(
                  children: [
                    const Expanded(child: SizedBox()),
                    datoWindget('Orden',pokemonController.pokemon.order.toString()),
                    const Expanded(child: SizedBox()),
                    datoWindget('Experiencia base',pokemonController.pokemon.baseExperience.toString()),
                    const Expanded(child: SizedBox()),
                  ],
                ),
                SizedBox(height: sizedHeight(context, 3),),
    
              ],
            )
          ),
        ],
      ),
    );
  }

  Column datoWindget(String titulo,String dato) {
    return Column(
      children:   [
        Text(titulo,style: AppTheme.lblNormal2,),
        const SizedBox(height: 5,),
        Text(dato,style: AppTheme.lblNormal,),
      ],
    );
  }

  SingleChildScrollView statsWidget(BuildContext context, PokemonController pokemonController ) {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(height: sizedHeight(context, 3),),
          Card(
            elevation: 5,
            child: Column(
              children: [
                SizedBox(height: sizedHeight(context, 3),),              
                statWindget(context,"Vida",pokemonController.pokemon.hp!),
                statWindget(context,"Ataque",pokemonController.pokemon.attack!),
                statWindget(context,"Defensa",pokemonController.pokemon.defense!),
                statWindget(context,"Atq. Especial",pokemonController.pokemon.specialAttack!),
                statWindget(context,"Def. Especial",pokemonController.pokemon.specialDefense!),
                statWindget(context,"Velocidad",pokemonController.pokemon.speed!),
                SizedBox(height: sizedHeight(context, 3),),
            
              ],
            )
          ),
        ],
      ),
    );
  }

  Row statWindget(BuildContext context, String titulo,int dato) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children:   [
        SizedBox(
          height: 40,
          width: sizedWith(context, 18),
          child: Center(child: Text(titulo ,style: AppTheme.lblNormal2,textAlign: TextAlign.center,)),
        ),
        SizedBox(width: sizedWith(context, 2),),
        SizedBox(
          width: sizedWith(context, 10),
          child: Text(dato.toString(),style: AppTheme.lblNormal),
        ),
        SizedBox(
          width: sizedWith(context, 54),
          child: SliderTheme(
            child: Slider(
              value: (dato<150)?dato.toDouble():150,
              max: 150,
              min: 0,
              activeColor: AppTheme.azul,
              inactiveColor: Colors.black12,
              onChanged: (double value) {},
            ),
            data: SliderTheme.of(context).copyWith(
              trackHeight: 10,
              thumbColor: Colors.transparent,
              thumbShape:const RoundSliderThumbShape(enabledThumbRadius: 0.0),
              overlayShape: SliderComponentShape.noOverlay,
                  
            ),       
          ),
        )
      ],
    );
  }

  Row opcionesWidget(BuildContext context, PokemonController pokemonController) {

    return Row(
      children: [
        const SizedBox(width: 20,),
        Expanded (
          child: SizedBox (
            height: 50,
            child: InkWell(
              onTap: () => pokemonController.clickDatos(),
              child: Column(
                children: [
                  Expanded(
                    child: Center(
                      child: Text("Datos",
                        style: (!pokemonController.stats) ?
                        AppTheme.lblNormal:
                        AppTheme.lblNormal2,
                      ),
                    ),
                  ),(!pokemonController.stats) ?
                    const Divider(height: 1,color: AppTheme.primary,):
                    Container(),
                ],
              ),
            ),
          ),
        ),
        Expanded(
          child: SizedBox (
            height: 50,
            child: InkWell(
              onTap: () => pokemonController.clickStats(),
              child: Column(
                children: [
                  Expanded(
                    child: Center(
                      child: Text("Estadisticas",
                        style: (pokemonController.stats) ?
                        AppTheme.lblNormal:
                        AppTheme.lblNormal2,
                      ),
                    ),
                  ),(pokemonController.stats) ?
                    const Divider(height: 1,color: AppTheme.primary,):
                    Container(),
                ],
              ),
            ),
          ),
        ),
        const SizedBox( width: 20)
      ],
    );
  }

}