import 'package:flutter/material.dart';

import '../router/resources.dart';


class UserPage extends StatelessWidget {
  
  const UserPage({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    final entrenadorController = Provider.of<EntrenadorController>(context);
    return  Scaffold(
      body:  Stack(
        children: [
          backgroundWidget(),
          titleWidget(context),
          Center(
            child: Image.asset('assets/imgs/user/entrenadores.png',width: sizedWith(context, 50),),
          ),
          Positioned(
            bottom: 50,
            right: sizedWith(context,5),
            child: SizedBox(
              width: sizedWith(context, 90),
              height: 200,
              child: Card(
                elevation: 10,
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      opcionesWidget(entrenadorController),
                      entrenadorController.creandoEntrenador?
                      usuarioNuevoWidget(context, entrenadorController):
                      seleccionarUsuarioWidget(context, entrenadorController)
                    ],
                  ),
                ),
              ),
            )
          )
        ],
      ),
    );
  }

  SizedBox seleccionarUsuarioWidget(BuildContext context, EntrenadorController entrenadorController) {
    
    return SizedBox(
      width: sizedWith(context, 90),
      height: 140,
      child: Column(
        children: [
          const Expanded(child: SizedBox()),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: FutureBuilder(
              future: entrenadorController.entrenadores,
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                return snapshot.hasData?//Container(height: 5,width: 5,color: Colors.amberAccent,) ;
                  DropdownButton<String>(
                  isExpanded: true,
                  value: entrenadorController.entrenador,
                  onChanged: (String? newValue)=>
                    entrenadorController.changeEntrenador(newValue.toString()),
                  style: entrenadorController.entrenador == 'seleccione entrenador' ?
                    AppTheme.lblNormal2:
                    AppTheme.lblNormal,
                  items:  snapshot.data.map<DropdownMenuItem<String>>((value) {
                    return DropdownMenuItem<String>(
                      value: value.toString(),
                      child: Text(value.toString()),
                    );
                  }).toList(),
                ):
                const Text("Consultando usuarios ...");
              },
            ),
              
            
          ),
          entrenadorController.entrenador == 'seleccione entrenador' ? 
          Container(height: 50,):
          Row(
            children: [
              const Expanded(child: SizedBox()),
              ElevatedButton(
                onPressed: () => entrenadorController.activarEntrenador(context), 
                child: const Text("INICIAR"),
                style: ElevatedButton.styleFrom(
                  primary: AppTheme.verde,
                  minimumSize:  Size(sizedWith(context, 35), 40),
                ),   
              ),
              const Expanded(child: SizedBox()),
              ElevatedButton(
                onPressed: () => entrenadorController.eliminarEntrenador(context), 
                child: const Text("ELIMINAR"),
                style: ElevatedButton.styleFrom(
                  primary: AppTheme.rojo,
                  minimumSize:  Size(sizedWith(context, 35), 40),
                ),   
              ),
              const Expanded(child: SizedBox()),
            ],
          ),
          const Expanded(child: SizedBox()),
        ],
      ),
    );
  }

  SizedBox usuarioNuevoWidget(BuildContext context,EntrenadorController entrenadorController) {
    return SizedBox(
      width: sizedWith(context, 90),
      height: 140,
      child: Column(
        children: [
          const Expanded(child: SizedBox()),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: sizedWith(context, 3)),
            child: TextField(
              style: AppTheme.lblNormal, 
              onChanged: (value) => entrenadorController.changeNombreNuevoEntrenador(value) ,                           
              decoration: const InputDecoration(
                hintStyle: AppTheme.lblNormal2,
                hintText: "Nombre de usuario"
              ),
            ),
          ),
          const Expanded(child: SizedBox()),
          ElevatedButton(
            onPressed: () => entrenadorController.crearEntrenador(context), 
            child: const Text("CREAR"),
            style: ElevatedButton.styleFrom(
              minimumSize:  Size(sizedWith(context, 80), 40),
            ),   
          ),
          const Expanded(child: SizedBox()),

        ],
      ),
    );
  }

  Positioned titleWidget(BuildContext context) {
    return Positioned(
      top: 80,
      right: sizedWith(context, 5), 
      child:Image.asset('assets/imgs/user/title.png',width: sizedWith(context, 90),),
    );
  }

  Container backgroundWidget() {
    return Container(
      decoration: const BoxDecoration(
        image: DecorationImage( 
          image: AssetImage('assets/imgs/user/back.jpeg'),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Row opcionesWidget(EntrenadorController entrenadorController) {
    return Row(
      children: [
        const SizedBox(width: 12,),
        Expanded (
          child: SizedBox (
            height: 50,
            child: InkWell(
              onTap: () => entrenadorController.changeCreandoEntrenador(),
              child: Column(
                children: [
                  Expanded(
                    child: Center(
                      child: Text("Nuevo usuario",
                        style: (entrenadorController.creandoEntrenador) ?
                        AppTheme.lblNormal:
                        AppTheme.lblNormal2,
                      ),
                    ),
                  ),(entrenadorController.creandoEntrenador) ?
                    const Divider(height: 1,color: AppTheme.primary,):
                    Container(),
                ],
              ),
            ),
          ),
        ),
        Expanded(
          child: SizedBox (
            height: 50,
            child: InkWell(
              onTap: () => entrenadorController.changeCreandoEntrenador(),
              child: Column(
                children: [
                  Expanded(
                    child: Center(
                      child: Text("Usuario existente",
                        style: (!entrenadorController.creandoEntrenador) ?
                        AppTheme.lblNormal:
                        AppTheme.lblNormal2,
                      ),
                    ),
                  ),(!entrenadorController.creandoEntrenador) ?
                    const Divider(height: 1,color: AppTheme.primary,):
                    Container(),
                ],
              ),
            ),
          ),
        ),
        const SizedBox( width: 12)
      ],
    );
  }

}