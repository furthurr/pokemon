import 'package:flutter/material.dart';

import '../router/resources.dart';



class EntrenadorController with ChangeNotifier{

  //VARIABLES:

  bool _creandoEntrenador = true;
  List<String> _entrenadores =  [];
  String _entrenador = 'seleccione entrenador';
  String _nombreEntrenadorNew = "";
  
  //OBTENER VARIABLES

  bool get creandoEntrenador =>_creandoEntrenador;
  String get entrenador => _entrenador;
  Future<List<String>> get entrenadores async{
    List<Entrenador> entrenadores = await DB.entrenadores();
    _entrenadores =  ['seleccione entrenador'];
    for (var entrenador in entrenadores) {
      _entrenadores.add(entrenador.nombre.toString());
    }
    return _entrenadores;
  } 


  //FUNCIONES:

  void changeCreandoEntrenador(){
    _creandoEntrenador = !_creandoEntrenador;
    notifyListeners();   
  }

  void changeNombreNuevoEntrenador(String nombre){
    _nombreEntrenadorNew = nombre;
  }

  void changeEntrenador(String entrenador){
    _entrenador = entrenador;
    notifyListeners();   
  }

  Future<void> eliminarEntrenador(BuildContext context )async{
    await DB.delete(_entrenador);
    msgAlerta(context, 'Eliminado', 'El entrenador "$_entrenador" y sus equipos fueron eliminados correctamente.');
    _entrenador='seleccione entrenador';
    notifyListeners();
  }

  Future<void> activarEntrenador(BuildContext context)async{
    await DB.activarEntrenador(_entrenador);
    _entrenador='seleccione entrenador';
    notifyListeners();
    Navigator.pushReplacement(
      context, 
      PageRouteBuilder(
        pageBuilder: (context, animation1, animation2) => const Homepage(),
        transitionDuration: Duration.zero,
      ),
    );
  }

  Future<void> crearEntrenador(BuildContext context)async{
    if (_nombreEntrenadorNew != ""){
      Entrenador entrenador = Entrenador(nombre:_nombreEntrenadorNew,status: true);
      entrenador = await DB.nuevoEntrenador(entrenador);
      await DB.activarEntrenador(entrenador.nombre.toString());
      _entrenador='seleccione entrenador';
      Navigator.pushReplacement(
        context, 
        PageRouteBuilder(
          pageBuilder: (context, animation1, animation2) => const Homepage(),
          transitionDuration: Duration.zero,
        ),
      );
    }else{
      msgAlerta(context, 'Error', 'Es necesario escribir el nombre del nuevo entrenador.');
    }
    
  }

}