
import 'package:flutter/material.dart';

import '../router/resources.dart';


class ListaController with ChangeNotifier{

  //VARIABLES:

  List<String> _listaPokemones = []; 
  bool  _creandoGrupo          = false;
  String  _nombreGrupo         = "";
  List<String> _grupoPokemon = [];
  
  //OBTENER VARIABLES


  Future<List<String>> get listaPokemones async{
    _listaPokemones = await ApiProvider.cargarLista();
    return _listaPokemones;
  }
  String       get nombreGrupo    => _nombreGrupo;
  List<String> get grupoPokemon   => _grupoPokemon;
  bool         get creandoGrupo   => _creandoGrupo;


  //FUNCIONES:

  void actualizaNombreGrupo(String nombre){
    _nombreGrupo = nombre;
  }

  void clickPokemon(BuildContext context,int idPokemon){
    infoprint(idPokemon);
    final pokemoncontroller = Provider.of<PokemonController>(context,listen: false);
    pokemoncontroller.setIDpokemon(idPokemon);
    Navigator.pushNamed(context, 'pokemon');
  }

  //Metodo para eliminar o agrega run elemento al grupo de pokemones
  void clickAgruparPokemon( String index){
    infoprint(index);
      _grupoPokemon.contains(index)?
      _grupoPokemon.remove(index):
      (_grupoPokemon.length<6)?
        _grupoPokemon.add(index)
        :null;
      notifyListeners();
    infoprint(_grupoPokemon);
  }


  void clickGuardarGrupo(BuildContext context) async{ 
    if(_grupoPokemon.length == 6 && _nombreGrupo.isNotEmpty){
      await DB.equiposInsert(_grupoPokemon.join(',').toString(), _nombreGrupo);
      msgAlerta(context, 'Creado!', 'El equipo "$_nombreGrupo" fue creado correctamente.');
      _nombreGrupo='';_grupoPokemon=[];
      clickCrearGrupo();
    }else{
      errorprint("Se tiene que crear grupo de 6 pokemones y es necesario un nombre del equipo");
      msgAlerta(context, 'Error', 'Es necesario seleccionar 6 pokemones y escribir el nombre de tu equipo.');
    }
  }

  void clickCrearGrupo(){
    _creandoGrupo = !_creandoGrupo;
    _grupoPokemon = [];
    notifyListeners();
  }

  void resetGrupo(){
    _creandoGrupo = false;
    _grupoPokemon = [];
  }

}