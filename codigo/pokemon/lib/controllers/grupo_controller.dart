
import 'package:flutter/material.dart';
import 'package:pokemon/router/resources.dart';


class GrupoController with ChangeNotifier{

  //VARIABLES:

  List<Equipo> _grupos =  [];
  
  //OBTENER VARIABLES

  Future<List<Equipo>> get grupos async{
    _grupos = await  DB.misEquipos();
    return _grupos;
  } 
  

  void clickEliminar( BuildContext context, int? idEquipo ){
    infoprint(idEquipo);
    var alert =AlertDialog(
      title: Row(
        children: [
          Image.asset('assets/imgs/pokeball.gif',height: 50,),
          const Text("Eliminar Equipo"),
          const Expanded(child: SizedBox())
        ],
      ),
      content: const Text("Seguro que deseas eliminarlo?."),
      actions: [
        TextButton(
          child: const Text("Eliminar"),
          onPressed: () async{
            await DB.eliminaEquipo(idEquipo);
            _grupos = await  DB.misEquipos();
            notifyListeners();
            Navigator.pop(context);
          } 
        ),
        TextButton(
          child: const Text("Cancelar"),
          onPressed: () => Navigator.pop(context),
        ),
      ],
  );
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );

  }

}