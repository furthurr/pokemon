import 'package:flutter/material.dart';
import 'package:pokemon/router/app_routes.dart';
import 'package:pokemon/router/resources.dart';


void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  final String ruta = await DB.entrenadorLogeado()?'home':'user';
  runApp( MyApp(ruta:ruta));
} 

class MyApp extends StatelessWidget {
  final String? ruta;
  const MyApp ({ Key? key, required this.ruta}) : super (key:key);
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => EntrenadorController()),
        ChangeNotifierProvider(create: (context) => ListaController()),
        ChangeNotifierProvider(create: (context) => PokemonController()),
        ChangeNotifierProvider(create: (context) => HomeController()),
        ChangeNotifierProvider(create: (context) => GrupoController()),
      ],
      //create: (context) => UserController(),

      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Pokemon',
        initialRoute: ruta,
        routes: AppRoutes.routes,
        theme: AppTheme.ligthTheme
      ),
    );
  }
}