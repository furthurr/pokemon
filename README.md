# Pokemon @furthurr
Pedro Gomez Vasquez
# Diseño
![](https://gitlab.com/furthurr/pokemon/-/raw/main/Dise%C3%B1o/ejemploPokemon.jpg)

## Login
| ![](https://gitlab.com/furthurr/pokemon/-/raw/main/Dise%C3%B1o/ejemploIphone/loginCel.png) | ![](https://gitlab.com/furthurr/pokemon/-/raw/main/Dise%C3%B1o/ejemploIphone/login2Cel.png) |
| - | - |

- Se puede iniciar la aplicación creando un nuevo usuario el cual se guardará en una base de datos sqlite en el dispositivo.
- Si el usuario ya existe podrá iniciar desde una lista de usuarios guardados en el dispositivo.


## Menu
 ![](https://gitlab.com/furthurr/pokemon/-/raw/main/Dise%C3%B1o/ejemploIphone/menuCel.png)

- Después de login el usuario contará con un drawer con las siguientes opciones:
- [x] Ver la lista de pokemones (opción default).
- [x] Ver los equipos de de pokemones creados por el usuario.
- [x] Hacer logout

## Lista de pokemones y crear equipos
| ![](https://gitlab.com/furthurr/pokemon/-/raw/main/Dise%C3%B1o/ejemploIphone/listaCel.png) | ![](https://gitlab.com/furthurr/pokemon/-/raw/main/Dise%C3%B1o/ejemploIphone/crearGrupoCel.png) |
| - | - |

- Lista con los primeros 150 pokemones.
- Al hacer click en el botón flotante podrá seleccionar 6 pokemones para crear un equipo.

## Equipos
| ![](https://gitlab.com/furthurr/pokemon/-/raw/main/Dise%C3%B1o/ejemploIphone/equiposCel.png) | ![](https://gitlab.com/furthurr/pokemon/-/raw/main/Dise%C3%B1o/ejemploIphone/eliminarEquipoCel.png) |
| - | - |

- Se mostrarán todos los equipos existentes junto con sus pokemones.
- Opción para  eliminar equipo.

## Pokemon
| ![](https://gitlab.com/furthurr/pokemon/-/raw/main/Dise%C3%B1o/ejemploIphone/pokemonCel.png) | ![](https://gitlab.com/furthurr/pokemon/-/raw/main/Dise%C3%B1o/ejemploIphone/pokemon2Cel.png) |
| - | - |

- Datos y estadísticas del pokemon.


## Author

* **Pedro Gomez** - *@furthurr* - [twitter](https://twitter.com/furthurr)
 
## License

Proyecto con licencia Apache 2.0 -

## Help me

* Invitame un coffee [paypal](https://www.paypal.me/furthurr/50)
* Mandame un teew [tweet](https://twitter.com/intent/tweet?related=furthurr)